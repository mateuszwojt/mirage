#include "utils/MathUtils.h"
#include "shaders/Material.h"
#include "Mesh.h"

namespace Mirage
{
    enum Type
    {
        eSphere,
        ePlane,
        eMesh
    };

    struct SphereGeometry
    {
        float radius;
    };

    struct PlaneGeometry
    {
        float plane[4];
    };

    struct MeshGeometry
	{

		const Vec3* positions;
		const Vec3* normals;
		const int* indices;
        const Vec2* uvs;
		const BVHNode* nodes;
		const float* cdf;

        const Material* material;

		int numVertices;
		int numIndices;
		int numNodes;

		float area;

		unsigned long id;
	};

    struct Primitive
    {
        Primitive() : lightSamples(0) {}

        // begin end transforms for the primitive
        Transform startTransform;	
        Transform endTransform;

        union
        {
            SphereGeometry sphere;
            PlaneGeometry plane;
            MeshGeometry mesh;
        };

        Type type;

        Material material;

        // if > 0 then primitive will be explicitly sampled
        int lightSamples;
    };
}
