#pragma once

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <memory>

#include "prims/Primitive.h"
#include "prims/Mesh.h"
#include "camera/Camera.h"
#include "shaders/Texture.h"
#include "shaders/Material.h"
#include "utils/MathUtils.h"

#include "BVH.h"
#include "Probe.h"


namespace Mirage
{
	class Camera;

	struct Light : Primitive
	{
		Light() { type = eSphere; }
		
		Vec3 position;
		Vec3 emission;
		
		float area;
	};

	struct Sky : Light
	{
		Vec3 horizon;
		Vec3 zenith;

		Probe probe;

		CUDA_CALLABLE Vec3 Eval(const Vec3& dir) const
		{
			if (probe.valid)
			{
				return ProbeEval(probe, ProbeDirToUV(dir));
			}
			else
			{
				return Lerp(horizon, zenith, sqrtf(Abs(dir.y)));
			}
		}
	};

	class Scene
	{
	public:
		Scene() : camera(nullptr) {};
		~Scene() = default;

		// Camera
		std::unique_ptr<Camera> camera;

		// Primitives
		std::vector<Primitive> primitives;
		
		// Meshes
		std::vector<std::unique_ptr<Mesh>> meshes;

		Sky sky;

		BVH bvh;

		void Clear();

		void AddPrimitive(const Primitive& prim);
		void AddMesh(std::unique_ptr<Mesh> mesh);

		void Build();

	private:
		std::mutex mutex;
	};
}