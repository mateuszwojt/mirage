#pragma once

#include "Vec3.h"

namespace Mirage
{
    CUDA_CALLABLE inline void ProjectPointToLine(Vec2 p, Vec2 a, Vec2 b, Real& t)
	{

	}

	CUDA_CALLABLE inline Vec2 ClosestPointToLineSegment(Vec2 p, Vec2 a, Vec2 b, Real& t)
	{
		Vec2 edge = b-a;
		Real edgeLengthSq = LengthSq(edge);
		
		if (edgeLengthSq == 0.0)
		{
			// degenerate edge, return first vertex
			t = 0.0;
			return a;
		}

		Vec2 delta = p-a;
		t = Dot(delta, edge)/edgeLengthSq;

		if (t <= 0.0)
		{
			return a;
		}
		else if (t >= 1.0)
		{
			return b;
		}
		else
		{
			return a + t*(b-a);
		}
	}

	// generates a transform matrix with v as the z axis, taken from PBRT
	CUDA_CALLABLE inline void BasisFromVector(const Vec3& w, Vec3* u, Vec3* v)
	{
		if (fabs(w.x) > fabs(w.y))
		{
			Real invLen = 1.0 / sqrt(w.x*w.x + w.z*w.z);
			*u = Vec3(-w.z*invLen, 0.0f, w.x*invLen);
		}
		else
		{
			Real invLen = 1.0 / sqrt(w.y*w.y + w.z*w.z);
			*u = Vec3(0.0f, w.z*invLen, -w.y*invLen);
		}

		*v = Cross(w, *u);	
	}


	CUDA_CALLABLE inline Vec3 UniformSampleSphere(float u1, float u2)
	{
		float z = 1.f - 2.f * u1;
		float r = sqrtf(Max(0.f, 1.f - z*z));
		float phi = 2.f * kPi * u2;
		float x = r * cosf(phi);
		float y = r * sinf(phi);

		return Vec3(x, y, z);
	}



	CUDA_CALLABLE inline Vec3 UniformSampleHemisphere(Random& rand)
	{
		// generate a random z value
		float z = rand.Randf(0.0f, 1.0f);
		float w = sqrt(1.0f-z*z);

		float phi = k2Pi*rand.Randf(0.0f, 1.0f);
		float x = cosf(phi)*w;
		float y = sinf(phi)*w;

		return Vec3(x, y, z);
	}

	CUDA_CALLABLE inline Vec2 UniformSampleDisc(float u1, float u2)
	{
		float r = sqrt(u1);
		float theta = k2Pi*u2;

		return Vec2(r * cos(theta), r * sin(theta));
	}

	CUDA_CALLABLE inline void UniformSampleTriangle(Random& rand, float& u, float& v)
	{
		float r = sqrt(rand.Randf());
		u = 1.0f - r;
		v = rand.Randf() * r;
	}

	CUDA_CALLABLE inline Vec3 CosineSampleHemisphere(float u1, float u2)
	{
		Vec2 s = UniformSampleDisc(u1, u2);
		float z = sqrt(Max(0.0f, 1.0f - s.x*s.x - s.y*s.y));

		return Vec3(s.x, s.y, z);
	}

	CUDA_CALLABLE inline Vec3 SphericalToXYZ(float theta, float phi)
	{
		float cosTheta = cos(theta);
		float sinTheta = sin(theta);

		return Vec3(sin(phi)*sinTheta, cosTheta, cos(phi)*sinTheta);
	}

	// make v lie in the same hemisphere as n
	CUDA_CALLABLE inline Vec3 FaceForward(const Vec3& n, const Vec3& v)
	{
		if (Dot(v, n) < 0.0f)
			return -n;
		else
			return n;
	}
} // namespace Mirage
