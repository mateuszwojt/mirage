#pragma once

#include <vector>

namespace Mirage
{	
	class Texture
	{
	public:
		Texture() : data(nullptr), width(0), height(0), depth(0) {};

		// Texture Data
		float* data;
		
		int width, height, depth;
	};
}
