# Mirage Path Tracer

This implementation is based on Miles Macklins' Tinsel renderer (https://github.com/mmacklin/tinsel)

Mirage is built only as a static library. There's no GUI or frontend application that can be used to render images.

## Requirements

- OpenMP (optional) - enables multithreaded rendering on the CPU
- CUDA toolkit (optional) - enables running the renderer on the NVidia GPUs, tested with CUDA toolkit 11.2

## Building

To compile Mirage with CUDA support, make sure `USE_CUDA` is on:
```
mkdir build && cd build
cmake -DUSE_CUDA=1 ..
make
make install
```

You can compile it without CUDA support, but the renderer will only be able to run in CPU mode.
