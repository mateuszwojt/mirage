cd $CI_PROJECT_DIR
mkdir build && cd build
cmake -DUSE_CUDA=1 ..
make
make install
cd ..
