#include "core/Scene.h"
#include "core/Intersection.h"

namespace Mirage
{
    void Scene::Clear()
    {
        meshes.clear();
        primitives.clear();

        delete bvh.nodes;
        bvh.nodes = nullptr;

        sky = Sky();
    }

    void Scene::AddPrimitive(const Primitive& prim)
    {
        primitives.push_back(prim);
    }

    void Scene::AddMesh(std::unique_ptr<Mesh> mesh)
    {
        if (mesh) {
            std::lock_guard<std::mutex> lock(mutex);
            meshes.push_back(std::move(mesh));
        }
    }

    void Scene::Build()
    {
        if (primitives.size() == 0)
        {
            // nothing to build the scene from, should yield some error
            return;
        }

        // build scene bvh
        std::vector<Bounds> primitiveBounds;

        for (size_t i=0; i < primitives.size(); ++i)
        {
            Bounds r = PrimitiveBounds(primitives[i]);
            primitiveBounds.push_back(r);
        }

        BVHBuilder builder;
        bvh = builder.Build(&primitiveBounds[0], primitiveBounds.size());
    }
}